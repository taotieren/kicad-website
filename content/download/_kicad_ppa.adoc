To install KiCad via the PPA, you can use the Software Manager:

1. Open the Software Manager.
2. Select 'Edit' -> 'Software Sources...'.
3. Open the 'Other Software' tab.
4. Click 'Add...', and enter the PPA address: `ppa:kicad/kicad-6.0-releases` and then click the
   'Add Source' button.
5. When prompted insert the administrator user password.
6. Return to the Software Manager and view the Progress tab to see when the cache has finished
   updating.
7. Return to the Software Manager main screen, search for 'kicad', and install it.

If you prefer to use the shell, you can enter these commands into a terminal:

[source,bash]
sudo add-apt-repository --yes ppa:kicad/kicad-6.0-releases
sudo apt update
sudo apt install --install-recommends kicad

This will perform a full installation of KiCad. 

== Nightly Development Builds

The _nightly development_ builds are snapshots of the development (master branch) codebase at a specific time.
This codebase is under active development, and while we try our best, may contain more bugs than usual.
New features added to KiCad can be tested in these builds.

WARNING: Please read link:/help/nightlies-and-rcs/[Nightly Builds and Release Candidates] for
		   important information about the risks and drawbacks of using nightly builds.

TIP: Nightly builds are provided in a separate installation directory. It is
possible to install nightly builds at the same time as a stable version.

Nightly development builds are available in
https://launchpad.net/~kicad/+archive/ubuntu/kicad-dev-nightly[Nightly PPA].
To install KiCad via the PPA, you can use the Ubuntu Software Manager:

1. Open the Software Manager.
2. Select 'Edit' -> 'Software Sources...'.
3. Open the 'Other Software' tab.
4. Click 'Add...', and enter the PPA address: `ppa:kicad/kicad-dev-nightly` and then
   click the 'Add Source' button.
5. When prompted insert the administrator user password.
6. Return to the Software Manager and view the Progress tab to see when the cache has
   finished updating.
7. Return to the Software Manager main screen, search for 'kicad-nightly', and install it.

If you prefer to use the shell, you can enter these commands into a terminal:

[source,bash]
sudo add-apt-repository --yes ppa:kicad/kicad-dev-nightly
sudo apt update
sudo apt install kicad-nightly
# You can also install debug symbols:
sudo apt install kicad-nightly-dbg
# Demo
sudo apt install kicad-nightly-demos
# and libraries
sudo apt install kicad-nightly-libraries

To launch nightly version of a kicad's binaries, you have to add "-nightly" to the
command name:

- kicad -> kicad-nightly
- pcbnew -> pcbnew-nightly
- eeschema -> eeschema-nightly
- ...
